![PS](https://media.discordapp.net/attachments/894982417511768084/1001548067511730326/cabecalho.png)

# Desafios PS 2022.2

 Sejam todos bem-vindos ao desafio para quem está concorrendo à vaga de desenvolvedor no processo seletivo 2022.2 do Vortex.
 
 Os desafios estão disponíveis na [WIKI](https://gitlab.com/camilamedeir0s/desafio-PS-2022.2/-/wikis/home), que se encontra na barra lateral da esquerda. 

## Regras básicas:

- O ideal é que cada um entregue, pelo menos, **2 desafios corretos**, mas quanto mais melhor;
- Incentivamos que vocês troquem experiências! Isso não quer dizer que aceitamos cópia de códigos. Pelo contrário, caso encontremos códigos similares entraremos em contato com os donos, e caso a dúvida seja confirmada todos que fizeram parte estão **automaticamente desclassificados**;
- Os desafios devem ser entregues em **um repositório** no gitlab (não esqueça de tornar o repositório público para que possamos acessar);
- Não somos apegados a nenhuma linguagem. Portanto, fique a vontade de responder os desafios nas linguagens que achar melhor. Pedimos só que indique a versão da linguagem está utilizando para que não tenhamos problema;
- Não é permitido o uso de bibliotecas para resolver os desafios;
- Caso não consiga resolver tudo, envie o que conseguir!

## Lembretes
- Você deve entregar o desafio até segunda-feira (08/08/2022) às **10:00 horas** por meio do [forms](https://docs.google.com/forms/d/e/1FAIpQLSem1NiVecm49KrqSVXqWqfslszV1eyE-s9v-L6KdHfT20CQTQ/viewform?usp=sf_link)

No mais, desejamos-lhe sorte, e esperamos comprometimento! Recomendamos não deixar para última hora. E só lembrando, estamos a disposição para que possam tirar dúvidas!
